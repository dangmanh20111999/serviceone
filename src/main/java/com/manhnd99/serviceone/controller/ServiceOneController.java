package com.manhnd99.serviceone.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/service-one")
public class ServiceOneController {

    @GetMapping
    public String getServiceOne() {
        return "Hello Service One Ver 1.1";
    }
}
